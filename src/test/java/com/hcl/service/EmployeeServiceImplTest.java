package com.hcl.service;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.hcl.dto.CreateEmployeeDto;
import com.hcl.model.Employee;
import com.hcl.repository.EmployeeRepository;

import junit.framework.Assert;

@RunWith(MockitoJUnitRunner.Silent.class)
public class EmployeeServiceImplTest {

	@InjectMocks
	EmployeeServiceImpl EmployeeServiceImpl;

	@Mock
	EmployeeRepository employeeRepository;

	static Employee employee = null;

	@BeforeClass
	public static void setUp() {
		employee = new Employee();
	}

	@Test
	public void testupdateEmployeeForPositive() {

		Employee employee = new Employee();

		employee.setEmail("kumar@gmail.com");
		employee.setEmployeeId((long) 1);
		employee.setEmployeeName("kumar");

		Mockito.when(employeeRepository.save(employee)).thenReturn(employee);
		Employee employeess = EmployeeServiceImpl.employeeUpdate(employee);
		Assert.assertNotNull(employeess);
		// Assert.assertEquals(1, employeess.getEmployeeId());
	}

	@Test
	public void testupdateEmployeeForNagative() {

		Employee employee = new Employee();
		// employee.setCompanyName("hcl");
		employee.setEmail("kumar@gmail.com");
		employee.setEmployeeId((long) 1);
		employee.setEmployeeName("kumar");
		// employee.setEmployeePhNo("-8838667354");
		Mockito.when(employeeRepository.save(employee)).thenReturn(employee);
		Employee employeess = EmployeeServiceImpl.employeeUpdate(employee);
		Assert.assertNotNull(employeess);
		// Assert.assertEquals(1, employeess.getEmployeeId());
	}

	@Test
	public void testcreateEmployeeForPositive() {

		Employee employee = new Employee();

		employee.setEmail("kumar@gmail.com");
		employee.setEmployeeId((long) 1);
		employee.setEmployeeName("kumar");

		Mockito.when(employeeRepository.save(employee)).thenReturn(employee);
		Employee employeess = EmployeeServiceImpl.employeeUpdate(employee);
		Assert.assertNotNull(employeess);

	}

	@Test
	public void testcreateEmployeeForNagative() {
		Employee employee = new Employee();

		employee.setEmail("kumar@gmail.com");
		employee.setEmployeeId((long) 1);
		employee.setEmployeeName("kumar");

		Mockito.when(employeeRepository.save(employee)).thenReturn(employee);
		Employee employeess = EmployeeServiceImpl.employeeUpdate(employee);
		Assert.assertNotNull(employeess);
	}

	@Test
	public void testgetAllEmployeeForPostive() {
		List<Employee> employeess = new ArrayList();
		Employee employee = new Employee();

		employee.setEmail("kumar@gmail.com");
		employee.setEmployeeId((long) 1);
		employee.setEmployeeName("kumar");

		Mockito.when(employeeRepository.save(employee)).thenReturn(employee);
		Employee employeess1 = EmployeeServiceImpl.employeeUpdate(employee);
		Assert.assertNotNull(employeess1);
	}

	@Test
	public void testgetAllEmployeeForNegative() {
		List<Employee> employeess = new ArrayList();
		Employee employee = new Employee();

		employee.setEmail("kumar@gmail.com");
		employee.setEmployeeId((long) 1);
		employee.setEmployeeName("kumar");

		Mockito.when(employeeRepository.save(employee)).thenReturn(employee);
		Employee employeess1 = EmployeeServiceImpl.employeeUpdate(employee);
		Assert.assertNotNull(employeess1);
	}
	@Test
	public void deleteEmployeeForPositive() {
		
		Employee emp =new Employee(198, "move","jhjj","fgh","dfgh");
		Mockito.when(employeeRepository.delete(employee)).thenReturn(employee);
		EmployeeServiceImpl.deleteEmployeeid((long) 12345);
		verify(employeeRepository, times(1)).deleteById(emp.getEmployeeId());
	}

	@AfterClass
	public static void tearDown() {
		employee = null;
	}
}
