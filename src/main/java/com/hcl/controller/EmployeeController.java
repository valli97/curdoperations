package com.hcl.controller;

import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.hcl.dto.CreateEmployeeDto;
import com.hcl.dto.ResponseMessageDto;
import com.hcl.exception.EmployeeNotFound;
import com.hcl.model.Employee;
import com.hcl.service.EmployeeService;
/**
 * 
 * @author ratnavalli
 * version:1.1
 * this is class for Employee controller
 *
 */
@RestController
public class EmployeeController {
	Logger logger = LoggerFactory.getLogger(EmployeeController.class);
	

	@Autowired
	EmployeeService employeeService;
	
	/**
	 * 
	 * @param name
	 * @return
	 * @throws EmployeeNotFound
	 */

	@GetMapping("/employee/employeeLike/{employeeName}")
	public  List<Employee> search(@PathVariable("employeeName") String employeeName) {
		logger.info("<-----------<search method>------------->");
		List<Employee> employee = employeeService.findByEmployeeNameLike(employeeName);
		if (employee != null) {
			return employee;
		} else {
			throw new EmployeeNotFound("the given name or letter is not found");
		}
	}
	

	@PostMapping(value = "/employee")
	public ResponseEntity<ResponseMessageDto> createEmployee(@RequestBody CreateEmployeeDto createEmployeeDto) {
		ResponseMessageDto responseMessageDto = new ResponseMessageDto();
		logger.info("<-----------<inside employee controller method>------------->");
		employeeService.createEmployee(createEmployeeDto);
		responseMessageDto.setMessage("Employee created  successfully");
		return new ResponseEntity<>(responseMessageDto, HttpStatus.OK);
	}

	@DeleteMapping(value = "/employee/{employeeid}")
	public ResponseEntity<ResponseMessageDto> deleteEmployee(@PathVariable("employeeid") Long employeeId) {
		ResponseMessageDto responseMessageDto = new ResponseMessageDto();
		logger.info("<-----------<inside deleteemployee Employee controller method>------------->");
		employeeService.deleteEmployeeid(employeeId);
		responseMessageDto.setMessage("Employee id is:" + employeeId + "deleted");
		return new ResponseEntity<>(responseMessageDto, HttpStatus.OK);
	}

	@GetMapping(value = "/employees")
	public  List<Employee> getAllEmployee() {
		logger.info("<-----------<inside GetAllEmployee Employee controller method>------------->");
		return employeeService.getAllEmployee();
	}
	

	@PutMapping("/employee")
	public ResponseEntity<Employee> employeeUpdate(@RequestBody Employee employee) {
		Employee employee1 = employeeService.employeeUpdate(employee);
		logger.info("<-----------<inside updateemployee Employee controller method>------------->");
		return new ResponseEntity<Employee>(employee1, HttpStatus.OK);
	}

	@GetMapping(value = "/employee/{employeeid}")
	public Employee getemployee(@PathVariable("employeeid") Long employeeId) {
		logger.info("<-----------<inside getById Employee controller method>------------->");
		Employee employee = employeeService.getemployee(employeeId);
		if (employee != null) {
			return employee;
		} else {
			throw new EmployeeNotFound("the given Id is not found");
		}

	}

}