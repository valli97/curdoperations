package com.hcl.service;

import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.ReturnedType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.hcl.dto.CreateEmployeeDto;
import com.hcl.dto.ResponseMessageDto;
import com.hcl.exception.EmployeeNotFound;
import com.hcl.exception.NoDataFound;
import com.hcl.model.Employee;
import com.hcl.repository.EmployeeRepository;

/**
 * 
 * @author ratnavalli this is class for Employee Service
 *
 */
@Service
public class EmployeeServiceImpl implements EmployeeService {
	Logger logger = LoggerFactory.getLogger(EmployeeServiceImpl.class);
	@Autowired
	EmployeeRepository employeeRepository;

	@Override
	public Employee createEmployee(CreateEmployeeDto createEmployeeDto) {
		Employee employee = new Employee();
		BeanUtils.copyProperties(createEmployeeDto, employee);
		return employeeRepository.save(employee);
	}

	@Override
	public List<Employee> getAllEmployee() {
		return employeeRepository.findAll();
	}

	
	@Override
	public void deleteEmployeeid(Long employeeId) {
		employeeRepository.deleteById(employeeId);

	}

	@Override
	public Employee getemployee(Long employeeId) {

		Optional<Employee> employee = employeeRepository.findById(employeeId);

		Employee emp = null;
		if (employee.isPresent()) {
			emp = employee.get();
		} else {
			throw new EmployeeNotFound("");
		}

		return emp;
	}

	@Override
	public Employee getEmployeeByName(String employeeName) {

		Employee employee = employeeRepository.getByEmployeeName(employeeName);
		if (employee != null) {
			return employee;
		} else {
			throw new EmployeeNotFound("");
		}
	}

	@Override
	public Employee employeeUpdate(Employee employee) {
		return employeeRepository.save(employee);

	}

	@Override
	public List<Employee> findByEmployeeNameLike(String employeeName) {
		List<Employee> employee = employeeRepository.findByEmployeeNameLike("%" + employeeName + "%");
		if (employee.isEmpty()) {
			throw new NoDataFound();
		}

		return employee;

	}

}