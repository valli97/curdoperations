package com.hcl.service;

import java.util.List;
import java.util.Optional;

import org.springframework.http.ResponseEntity;

import com.hcl.dto.CreateEmployeeDto;
import com.hcl.dto.ResponseMessageDto;
import com.hcl.model.Employee;

public interface EmployeeService {

	public List<Employee> getAllEmployee();

	// public Employee updateEmployee(Employee employee);

	public Employee employeeUpdate(Employee employee);

	public Employee createEmployee(CreateEmployeeDto createEmployeeDto);

	public void deleteEmployeeid(Long employeeId);

	public Employee getemployee(Long employeeId);

	public Employee getEmployeeByName(String employeeName);

	public List<Employee> findByEmployeeNameLike(String employeeName);
	// public List<Employee> getEmployeeByNameStartingWith(String employeeName);
	// public Employee findByNameContainingIgnoreCase(String employeeName);

}
