package com.hcl.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


import com.hcl.dto.CreateEmployeeDto;
import com.hcl.dto.ResponseMessageDto;
import com.hcl.model.Employee;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long>{

	public Employee getByEmployeeName(String employeeName);
	//public List<Employee> findByNameContainingIgnoreCase(String employeeName);
	//public Employee findByEmployeeNameContainingIgnoreCase(String employeeName);
	public List<Employee> findByEmployeeNameLike(String employeeName);
		
}